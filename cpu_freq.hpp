/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_CPU_FREQ_HPP
#define AVRTL_CPU_FREQ_HPP

#include "units/frequency.hpp"

namespace avrtl {

typedef units::Hz CPUFreq;
constexpr CPUFreq const cpu_freq{CPUFreq::rep(CPU_FREQ)};

} // namespaces avrtl

#endif // AVRTL_CPU_FREQ_HPP
