/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ATOMIC_INTERRUPTS_SCOPED_LOCK_HPP
#define AVRTL_ATOMIC_INTERRUPTS_SCOPED_LOCK_HPP

#include "interrupts_ctl.hpp"

namespace avrtl {
namespace atomic {

template<typename interrupts = interrupts_ctl>
class interrupts_scoped_lock {
public:
	interrupts_scoped_lock() {
		enableInterrupts = interrupts::are_enabled();
		interrupts::disable();
	}
	~interrupts_scoped_lock() {
		if(enableInterrupts) { interrupts::enable(); }
	}
	volatile bool enableInterrupts;
};

} // namespaces atomic
} // namespaces avrtl

#endif // AVRTL_ATOMIC_INTERRUPTS_SCOPED_LOCK_HPP
