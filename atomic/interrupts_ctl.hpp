/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ATOMIC_INTERRUPTS_CTL_HPP
#define AVRTL_ATOMIC_INTERRUPTS_CTL_HPP

namespace avrtl {
namespace atomic {

struct interrupts_ctl
{
	static void enable() { sei(); }
	static void disable() { cli(); }
	static bool are_enabled() { return (SREG | 0x80); }
};

} // namespaces atomic
} // namespaces avrtl

#endif // AVRTL_ATOMIC_INTERRUPTS_CTL_HPP
