/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ATOMIC_ATOMIC_HPP
#define AVRTL_ATOMIC_ATOMIC_HPP
#include "interrupts_scoped_lock.hpp"

#include "../utils/type_traits.hpp"

namespace avrtl {
namespace atomic {

namespace detail {
template<typename T>
class atomic_common
{
public:
	atomic_common() = default;
	constexpr atomic_common(T v) : val(v) { }
	atomic_common(const atomic_common&) = delete;
	atomic_common(atomic_common&&) = delete;

	atomic_common& operator=(const atomic_common&) = delete;
	atomic_common& operator=(const atomic_common&) volatile = delete;

	T operator=(T desired) { val = desired; return *this; }
	T operator=(T desired) volatile { val = desired; return *this; }
protected:
	T load_copy() const volatile { return detail::atomic_common<T>::val; }

	T val;
};

} // namespace

template<typename T, typename IsIntegral = utils::is_integral<T>>
class atomic : public detail::atomic_common<T>
{
public:
	atomic() = default;
	constexpr atomic(T v) : detail::atomic_common<T>(v) { }
	atomic(const atomic&) = delete;
	atomic(atomic&&) = delete;

	atomic& operator=(const atomic&) = delete;
	atomic& operator=(const atomic&) volatile = delete;

	constexpr bool is_lock_free() const volatile { return false; }

	operator T() const volatile { return load(); }

	T load() const volatile
	{
		interrupts_scoped_lock<> il;
		return detail::atomic_common<T>::load_copy();
	}

	void store(T new_val) volatile
	{
		interrupts_scoped_lock<> il;
		detail::atomic_common<T>::val = new_val;
	}
};

template<typename T>
class atomic<T, utils::true_type> : public detail::atomic_common<T>
{
public:
	atomic() = default;
	constexpr atomic(T v) : detail::atomic_common<T>(v) { }
	atomic(const atomic&) = delete;
	atomic(atomic&&) = delete;

	atomic& operator=(const atomic&) = delete;
	atomic& operator=(const atomic&) volatile = delete;

	constexpr bool is_lock_free() const volatile { return false; }

	operator T() const volatile { return load(); }

	T load() const volatile
	{
		interrupts_scoped_lock<> il;
		return detail::atomic_common<T>::load_copy();
	}

	void store(T new_val) volatile
	{
		interrupts_scoped_lock<> il;
		detail::atomic_common<T>::val = new_val;
	}

	T fetch_add(T arg) {
		interrupts_scoped_lock<> il;
		T copy = detail::atomic_common<T>::load_copy();
		detail::atomic_common<T>::val += arg;
		return copy;
	}
	T fetch_sub(T arg) {
		interrupts_scoped_lock<> il;
		T copy = detail::atomic_common<T>::load_copy();
		detail::atomic_common<T>::val -= arg;
		return copy;
	}
};


template<>
class atomic<uint8_t, utils::is_integral<uint8_t>> : public detail::atomic_common<uint8_t>
{
public:
	atomic() = default;
	constexpr atomic(uint8_t v) : detail::atomic_common<uint8_t>(v) { }
	atomic(const atomic&) = delete;
	atomic(atomic&&) = delete;

	atomic& operator=(const atomic&) = delete;
	atomic& operator=(const atomic&) volatile = delete;

	operator uint8_t() const volatile { return load(); };
	uint8_t load() const volatile { return val; };

	void store(uint8_t new_val) volatile { val = new_val; };

	constexpr bool is_lock_free() const volatile { return true; }

	uint8_t fetch_add(uint8_t arg) { return detail::atomic_common<uint8_t>::val += arg; }
	uint8_t fetch_sub(uint8_t arg) { return detail::atomic_common<uint8_t>::val -= arg; }
};

template<typename T>
class atomic<T*> {};
} // namespaces atomic
} // namespaces avrtl

#endif // AVRTL_ATOMIC_ATOMIC_HPP
