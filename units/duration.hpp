/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_DURATION_HPP
#define AVRTL_UTILS_DURATION_HPP

extern "C" {
#include <stdint.h>
}

#include "type_traits.hpp"
#include "../utils/ratio.hpp"

#include "detail/is_ratio.hpp"
#include "value.hpp"

namespace avrtl {
namespace units {

class Time;
// class template duration
template <typename Rep, typename Period = utils::ratio<1>>
class duration : public Value<Rep, Period, Time> {
public:
   static_assert(utils::detail::is_ratio<Period>::value, "Period shall be a specialization of ratio");
   static_assert(Period::num > 0, "Period shall be positive");

   typedef Period period;

   duration(const duration&) = default;
   constexpr explicit duration(const Rep& p_val) : Value<Rep, Period, Time>(p_val) {}

   constexpr Rep count() const { return this->val(); }
};

typedef duration<uint64_t, utils::micro>       microseconds;
typedef duration<uint64_t, utils::milli>       milliseconds;
typedef duration<uint64_t>                     seconds;
typedef duration<uint32_t, utils::ratio<60>>   minutes;
typedef duration<uint32_t, utils::ratio<3600>> hours;

template<typename TargetType, typename Rep, typename Period>
constexpr TargetType duration_cast(const duration<Rep, Period>& duration)
{
   typedef utils::ratio_divide<Period, typename TargetType::period> ratio_div;
   return TargetType(typename TargetType::rep(duration.count()) * ratio_div::num / ratio_div::den);
}

constexpr hours operator "" _h(long long unsigned hrs) { return hours(hrs); }
constexpr minutes operator "" _min(long long unsigned min) { return minutes(min); }
constexpr seconds operator "" _s(long long unsigned s) { return seconds(s); }
constexpr milliseconds operator "" _ms(long long unsigned ms) { return milliseconds(ms); }
constexpr microseconds operator "" _us(long long unsigned us) { return microseconds(us); }

} // namespaces units
} // namespaces avrtl

#endif // AVRTL_UTILS_DURATION_HPP
