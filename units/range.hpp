/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UNITS_RANGE_HPP
#define AVRTL_UNITS_RANGE_HPP

#include "../utils/type_traits.hpp"
#include "../utils/forward.hpp"
#include "value.hpp"

namespace avrtl {
namespace units {

template<typename T>
struct range {
   typedef T value_type;
   constexpr range(T min, T max) : m_min(min), m_max(max) {}
   constexpr T min() const { return m_min; }
   constexpr T max() const { return m_max; }
private:
   T m_min, m_max;
};

template<typename R, typename V> constexpr bool in_range(R&& range, V&& val)
{
   typedef typename utils::remove_reference<R>::type RangeType;
   typedef typename RangeType::value_type target_unit;
   return unit_cast<target_unit>(val) >= range.min() && unit_cast<target_unit>(val) <= range.max();
}

} // namespace units
} // namespace avrtl

#endif // AVRTL_UNITS_RANGE_HPP
