/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UNITS_VOLTAGE_HPP
#define AVRTL_UNITS_VOLTAGE_HPP

#include "../utils/ratio.hpp"
#include "value.hpp"

namespace avrtl {
namespace units {

class voltage;

using MiliVolts = Value<int32_t, utils::milli, voltage>;
using Volts = Value<int32_t, utils::ratio<1>, voltage>;

} // namespace units
} // namespace avrtl

#endif // AVRTL_UNITS_VOLTAGE_HPP
