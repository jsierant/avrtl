/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UNITS_CELSIUS_HPP
#define AVRTL_UNITS_CELSIUS_HPP

#include "../utils/ratio.hpp"
#include "value.hpp"

namespace avrtl {
namespace units {

class celsius;

using MiliCelcius = Value<int32_t, utils::milli, celsius>;
using Celcius = Value<int32_t, utils::ratio<1>, celsius>;

} // namespace units
} // namespace avrtl

#endif // AVRTL_UNITS_CELSIUS_HPP
