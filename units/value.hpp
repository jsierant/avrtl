/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UNITS_VALUE_HPP
#define AVRTL_UNITS_VALUE_HPP

#include "../utils/detail/is_ratio.hpp"

namespace avrtl {
namespace units {

namespace detail {
template<typename Representation, typename unit_scaler, typename d>
struct common_value
{
	typedef Representation rep;
	typedef unit_scaler unit_ratio;
	typedef d domain;
};
} // namespace detail

template<typename Representation, typename unit_scaler, typename d>
class Value : public detail::common_value<Representation, unit_scaler, d>
{
	static_assert(utils::detail::is_ratio<unit_scaler>::value, "unit_scaler shall be a type of ratio!!");
public:
	constexpr explicit Value(Representation p_val)  : repVal(p_val) { }
	constexpr Representation val() const { return repVal; }
	constexpr Value operator+(const Value& right) { return Value(repVal+right.repVal); }
	constexpr Value operator-(const Value& right) { return Value(repVal-right.repVal); }
	constexpr Value operator/(const Value& right) { return Value(repVal/right.repVal); }
	constexpr Value operator*(const Value& right) { return Value(repVal*right.repVal); }
	constexpr bool operator<(const Value& right) const { return repVal<right.repVal; }
	constexpr bool operator>(const Value& right) const { return repVal>right.repVal; }
	constexpr bool operator>=(const Value& right) const { return repVal>=right.repVal; }
	constexpr bool operator<=(const Value& right) const { return repVal<=right.repVal; }
	constexpr bool operator==(const Value& right) const { return repVal==right.repVal; }
private:
	Representation repVal;
};

template<typename T> constexpr float to_float(T&& v)
{
	return float(v.val() * T::unit_ratio::num) / T::unit_ratio::den;
}

template<typename target_type, typename rep, typename src_ratio, typename src_domain>
constexpr target_type unit_cast(const Value<rep, src_ratio, src_domain>& val)
{
	static_assert(utils::is_same<src_domain, typename target_type::domain>::value,
					  "Values only from the same domain can be casted!!");
	typedef utils::ratio_divide<src_ratio, typename target_type::unit_ratio> ratio_div;
   return target_type(typename target_type::rep(val.val()) * ratio_div::num / ratio_div::den);
}

} // namespace units
} // namespace avrtl

#endif // AVRTL_UNITS_VALUE_HPP
