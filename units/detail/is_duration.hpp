/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_DETAIL_IS_DURATION_HPP
#define AVRTL_UTILS_DETAIL_IS_DURATION_HPP

#include "../../utils/type_traits.hpp"
#include "../duration.hpp"

namespace avrtl {
namespace units {
namespace detail {

template<typename D> struct is_duration : utils::false_type {};
template<typename Rep, typename Period> struct is_duration<duration<Rep, Period> > : utils::true_type { };

} // namespaces detail
} // namespaces units
} // namespaces avrtl

#endif // AVRTL_UTILS_DETAIL_IS_DURATION_HPP
