/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_LCD_HD44780_LCD_HPP
#define AVRTL_LCD_HD44780_LCD_HPP

#include "instructions.hpp"

#include "../../utils/force_inline.hpp"
#include "../../delay/sleep_for.hpp"

namespace avrtl {
namespace lcd {
namespace hd44780 {

/**
 * template types have to be according to example (see: lcd::hd44780::example::line)
 *
 * @tparam RS - RS line
 * @tparam E  - E line
 * @tparam DB - DB data bus
 */
template < typename RS,
         typename E,
         typename DB >
class lcd {
public:
   lcd() {
      initPins();
      delay::sleep_for(units::milliseconds(1));
      RS::clear(); E::clear();
      DB::init();
      writeCmd(inst_function_set | inst_font5x7 | inst_two_line | DB::mode_inst);
      writeCmd(inst_display_onoff | inst_display_off);
      writeCmd(inst_clear);
      delay::sleep_for(units::milliseconds(2));
      writeCmd(inst_entry_mode | inst_em_shift_cursor | inst_em_increment);
      writeCmd(inst_display_onoff | inst_display_on | inst_cursor_off | inst_cursor_noblink);
   }
   void writeText(const char* text) const { while (*text) { writeData(*text++); } }
   void writeData(const char data) const { RS::set(); write(data);}
   inline void goTo(uint8_t x, uint8_t y) const { writeCmd(inst_ddram_set | (x + (0x40 * y))); }
   inline void clear() const { writeCmd(inst_clear); delay::sleep_for(units::milliseconds(1)); goTo(0u, 0u); }
   inline void home() const { writeCmd(inst_home); }
private:
   force_inline void initPins() {
      RS::init(); E::init(); DB::initPins();
   }
   void writeCmd(const uint8_t cmd) const { RS::clear(); write(cmd); }
   void write(const char data) const { DB::writeToBus(data); }
};

} // namespace hd44780
} // namespace lcd
} // namespace avrtl

#endif // AVRTL_LCD_HD44780_LCD_HPP
