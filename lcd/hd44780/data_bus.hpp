/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_LCD_HD44780_DATA_BUS_HPP
#define AVRTL_LCD_HD44780_DATA_BUS_HPP

extern "C"
{
#include <stdint.h>
}

#include "instructions.hpp"
#include "../../delay/sleep_for.hpp"

namespace avrtl {
namespace lcd {
namespace hd44780 {

template < typename E,
         typename DB4,
         typename DB5,
         typename DB6,
         typename DB7 >
class data_bus4 {
public:
   static const instructions mode_inst = inst_4_bit;
   static void init() {
      for (uint8_t i = 0; i < 3; ++i) {
         E::set(); writeToBus(0x03); E::clear();
      }
      E::set(); writeToBus(0x02); E::clear();
   }

   static void initPins() {
      DB4::init(); DB5::init(); DB6::init(); DB7::init();
   }
   static void writeToBus(const uint8_t value) {
      E::set(); writeToBus4Bits(value >> 4); E::clear();
      delay::sleep_for(units::microseconds(100));
      E::set(); writeToBus4Bits(value); E::clear();
      delay::sleep_for(units::microseconds(200));
   }
private:
   static void writeToBus4Bits(const uint8_t value) {
      DB4::clear(); DB5::clear(); DB6::clear(); DB7::clear();
      if (value & 0x01) { DB4::set(); }
      if (value & 0x02) { DB5::set(); }
      if (value & 0x04) { DB6::set(); }
      if (value & 0x08) { DB7::set(); }
   }
};

} // namespace hd44780
} // namespace lcd
} // namespace avrtl

#endif // AVRTL_LCD_HD44780_DATA_BUS_HPP
