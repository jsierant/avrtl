/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_LCD_HD44780_INSTRUCTIONS_HPP
#define AVRTL_LCD_HD44780_INSTRUCTIONS_HPP

namespace avrtl
{
namespace lcd
{
namespace hd44780
{

enum instructions
{
	inst_clear = 0x01,
	inst_home = 0x02,

	inst_entry_mode = 0x04,
	inst_em_shift_cursor = 0x0,
	inst_em_shift_display = 0x1,
	inst_em_decrement = 0x0,
	inst_em_increment =  0x2,

	inst_display_onoff = 0x08,
	inst_display_off = 0,
	inst_display_on = 4,
	inst_cursor_off = 0,
	inst_cursor_on = 2,
	inst_cursor_noblink = 0,
	inst_cursor_blink = 1,

	inst_display_cursor_shift = 0x10,
	inst_shift_cursor = 0,
	inst_shift_display = 8,
	inst_shift_left = 0,
	inst_shift_right = 4,

	inst_function_set = 0x20,
	inst_font5x7 = 0,
	inst_font5x10 = 4,
	inst_one_line = 0,
	inst_two_line = 8,
	inst_4_bit = 0,
	inst_8_bit = 16,

	inst_cgram_set = 0x40,
	inst_ddram_set = 0x80
};

} // namespace hd44780
} // namespace lcd
} // namespace avrtl

#endif // AVRTL_LCD_HD44780_INSTRUCTIONS_HPP
