/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_TYPE_TRAITS_HPP
#define AVRTL_UTILS_TYPE_TRAITS_HPP

#include <defs.hpp>

namespace avrtl {
namespace utils {

template <typename T, T v> struct integral_constant
{
  static constexpr T value = v;
  typedef T value_type;
  typedef integral_constant type;
  constexpr operator value_type() const { return value; }
  constexpr value_type operator()() const { return value; }
};

typedef integral_constant<bool, true> true_type;
typedef integral_constant<bool, false> false_type;

template<typename, typename> struct is_same : public false_type { };
template<typename T> struct is_same<T, T> : public true_type { };

template<typename T> struct remove_reference { typedef T type; };
template<typename T> struct remove_reference<T&> { typedef T type; };
template<typename T> struct remove_reference<T&&> { typedef T type; };

template<typename T> struct remove_const { typedef T type; };
template<typename T> struct remove_const<const T> { typedef T type; };
template<typename T> struct remove_volatile { typedef T type; };
template<typename T> struct remove_volatile<volatile T> { typedef T type; };
template<typename T> struct remove_cv : remove_volatile<typename remove_const<T>::type> { };

template<typename T> struct add_const { typedef T const type; };
template<typename T> struct add_volatile { typedef T volatile  type; };
template<typename T> struct add_cv { typedef typename add_const<typename add_volatile<T>::type>::type  type; };

template<typename> struct is_lvalue_reference : false_type {};
template<typename T> struct is_lvalue_reference<T&> : true_type {};

template<typename> struct is_rvalue_reference : false_type {};
template<typename T> struct is_rvalue_reference<T&&> : true_type {};

template<typename T> struct is_integral : false_type {};
template<> struct is_integral<bool> : true_type {};
template<> struct is_integral<char> : true_type {};
template<> struct is_integral<unsigned char> : true_type {};
template<> struct is_integral<char32_t> : true_type {};
// template<> struct is_integral<unsigned char32_t> : true_type {};
template<> struct is_integral<wchar_t> : true_type {};
// template<> struct is_integral<unsigned wchar_t> : true_type {};
template<> struct is_integral<short> : true_type {};
template<> struct is_integral<unsigned short> : true_type {};
template<> struct is_integral<int> : true_type {};
template<> struct is_integral<unsigned> : true_type {};
template<> struct is_integral<long> : true_type {};
template<> struct is_integral<unsigned long> : true_type {};
template<> struct is_integral<long long> : true_type {};
template<> struct is_integral<unsigned long long> : true_type {};


template <typename T> struct array_traits;
template <typename T, size_t N> struct array_traits<T[N]>
{
   static size_t const size = N;
   typedef T type;
};

template<typename T> struct is_array: false_type {};
template<typename T, size_t N> struct is_array<T[N]>: true_type {};

} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_TYPE_TRAITS_HPP
