/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_ARRAY_SIZE_HPP
#define AVRTL_UTILS_ARRAY_SIZE_HPP

#include "defs.hpp"

namespace avrtl {
namespace utils {

template<typename T, size_t size> constexpr size_t array_size(T (&)[size]) { return size; }

} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_ARRAY_SIZE_HPP
