/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_BITS_TRAITS_H
#define AVRTL_UTILS_BITS_TRAITS_H

#include <type_traits.hpp>

namespace avrtl
{
namespace utils
{

template<typename T, uint8_t num_of_bits_in_byte = 8>
struct num_of_bits : integral_constant<uint8_t, sizeof(T)*num_of_bits_in_byte>
{
	static_assert(is_integral<T>::value, "T shall be an integral type");
};

template<avrtl::utils::size_t bits, avrtl::utils::size_t csize = 0>
struct smallest_type_to_store_bits {
	static_assert(bits <= num_of_bits<uint32_t>::value, "bits must be able to store with uint32 or smaller type");
	typedef typename smallest_type_to_store_bits<bits, bits-1/num_of_bits<uint8_t>::value>::type type;
};

template<avrtl::utils::size_t bits> struct smallest_type_to_store_bits<bits, 0> { typedef uint8_t type; };
template<avrtl::utils::size_t bits> struct smallest_type_to_store_bits<bits, 1> { typedef uint16_t type; };
template<avrtl::utils::size_t bits> struct smallest_type_to_store_bits<bits, 2> { typedef uint32_t type; };
template<avrtl::utils::size_t bits> struct smallest_type_to_store_bits<bits, 3> { typedef uint32_t type; };

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_BITS_TRAITS_H
