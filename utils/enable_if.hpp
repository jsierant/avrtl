/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_ENABLE_IF_HPP
#define AVRTL_UTILS_ENABLE_IF_HPP

namespace avrtl {
namespace utils {

template <bool, class T = void> struct enable_if { };
template <class T> struct enable_if<true, T> { typedef T type; };

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_ENABLE_IF_HPP
