/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_MOVE_HPP
#define AVRTL_UTILS_MOVE_HPP


#include <type_traits.hpp>

namespace avrtl
{
namespace utils
{

template<typename T> constexpr typename remove_reference<T>::type&& move(T&& v)
{
	return static_cast<typename remove_reference<T>::type&&>(v);
}

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_MOVE_HPP
