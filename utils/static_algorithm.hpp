/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_STAT_STATIC_ALGORITHM_HPP
#define AVRTL_UTILS_STAT_STATIC_ALGORITHM_HPP

#include "type_traits.hpp"

namespace avrtl {
namespace utils {
namespace stat {

namespace {

namespace {

template<typename pred,
			bool is_same,
			uint8_t current_idx,
			typename T,
			T... sequence>
struct first_match_idx_calc;

template<typename pred,
			uint8_t current_idx,
			typename T,
			T first,
			T... rest>
struct first_match_idx_calc<pred, false, current_idx, T, first, rest...>
	: first_match_idx_calc<pred, pred::template apply<first>(), current_idx+1, T, rest...>
{ };

template<typename pred,
			uint8_t current_idx,
			typename T,
			T... sequence>
struct first_match_idx_calc<pred, true, current_idx, T, sequence...> {
	static constexpr uint8_t idx = current_idx;
	static constexpr uint8_t exists = true;
};

template<typename pred, uint8_t current_idx, typename T>
struct first_match_idx_calc<pred, false, current_idx, T>
{
	static constexpr uint8_t idx = 0;
	static constexpr uint8_t exists = false;
};
} // namespace detail


template<typename pred,
			typename T,
			T... sequence>
struct find_first_by_value;

template<typename pred,
			typename T,
			T first,
			T... rest>
struct find_first_by_value<pred, T, first, rest...>
	: first_match_idx_calc<pred, pred::template apply<first>(), 0, T, rest...>
{ };


template<typename pred,
			typename T,
			typename Vect>
struct find_idx_first_matching_vect;

template<typename pred,
			typename T,
			template<typename A, A...> class Vect,
			T... vals>
struct find_idx_first_matching_vect<pred, T, Vect<T, vals...>>
	: find_first_by_value<pred, T, vals...> { };

} // namespace

template<typename T, T val>
struct eq_c {
	template<T act> static constexpr bool apply() { return val==act; }
};

template<typename Vect,
			typename predicate>
struct contains_c
	: integral_constant<bool, find_idx_first_matching_vect<predicate, typename Vect::value_type, Vect>::exists>
{ };


template<typename Vect,
			typename predicate,
			typename found_result = find_idx_first_matching_vect<predicate, typename Vect::value_type, /*v,*/ Vect>>
struct find_first_c
	: integral_constant<uint8_t, found_result::idx>{
	static_assert(found_result::exists, "Can't find index of a requested value!!");
};

template<typename Vect, uint8_t idx> struct at_c;

template<template <typename T, T...> class Vect,
			uint8_t idx,
			typename val_type,
			val_type head,
			val_type... tail>
struct at_c<Vect<val_type, head, tail...>, idx> : at_c<Vect<val_type, tail...>, idx-1>
{ };


template<template <typename T, T...> class Vect,
			typename val_type,
			val_type head,
			val_type... tail>
struct at_c<Vect<val_type, head, tail...>, 0> : integral_constant<val_type, head> { };


template<int idx, typename... E> struct at;
template<int idx, typename H, typename... T> struct at<idx, H, T...> : at<idx-1, T...> { };
template<typename H, typename... T> struct at<0, H, T...> { typedef H type; };


} // namespace stat
} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_STAT_STATIC_ALGORITHM_HPP
