/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_FORWARD_HPP
#define AVRTL_UTILS_FORWARD_HPP

#include "type_traits.hpp"

namespace avrtl {
namespace utils {

template<typename T> constexpr T&& forward(typename remove_reference<T>::type& v) { return static_cast<T&&>(v); }
template<typename T> constexpr T&& forward(typename remove_reference<T>::type&& v) {
   static_assert(!is_lvalue_reference<T>::value, "The template arg shall not be a lvalue reference type!");
   return static_cast<T&&>(v); 
}

} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_FORWARD_HPP
