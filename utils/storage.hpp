/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_STORAGE_HPP
#define AVRTL_UTILS_STORAGE_HPP

#include <stdint.h>

namespace avrtl
{
namespace utils
{

typedef uintptr_t Address;

namespace detail
{
template<unsigned Idx, Address MinAddr, typename... T>
struct StorableTraits;

template<Address MinAddr, typename F, typename... T>
struct StorableTraits<0, MinAddr, F, T...>
{
    typedef typename F::type type;
    constexpr static const Address addr = MinAddr;
};

template<unsigned Idx, Address MinAddr, typename F, typename... T>	
class StorableTraits<Idx, MinAddr, F, T...>
{
  typedef StorableTraits<Idx-1, MinAddr, T...> StorageTraitsNext;
public:
    typedef typename StorageTraitsNext::type type;
    constexpr static const Address addr = StorageTraitsNext::addr + sizeof(typename F::type);
};

template<unsigned I, typename... T> class IdxValidator;
template<unsigned I, typename F, typename... T> struct IdxValidator<I, F, T...> { static const bool isValid = IdxValidator<I+1, T...>::isValid && F::idx == I; };
template<unsigned I, typename F> struct IdxValidator<I, F> { static const bool isValid = F::idx == I; };


template<Address MinAddr, typename... T> class MaxAddr;
template<Address MinAddr, typename F, typename... T> struct MaxAddr<MinAddr, F, T...> { constexpr static const Address value = MaxAddr<MinAddr, T...>::value + sizeof(typename F::type); };
template<Address MinAddr, typename F> struct MaxAddr<MinAddr, F> { constexpr static const Address value = MinAddr + sizeof(typename F::type); };

template<typename... T> struct IdxType;
template<typename F, typename... T> struct IdxType<F, T...> { typedef typename F::idxType type; };

} // namespace detail


class StorageAccessor
{
public:
	template<typename T, Address Addr> static T read() { return T(); }
	template<typename T, Address Addr> static void write(T) { }
};

template<Address Min, Address Max>
struct AddrRange {
	typedef Address type;
    constexpr static const Address min = Min;
    constexpr static const Address max = Max;
};

template<typename IdxType, typename IdxHelperType = uint8_t>
struct StoreHelper {
    template<IdxType Idx, typename S>
    struct store {
        static const IdxHelperType idx = Idx;
        typedef S type;
        typedef IdxType idxType;
    };
};

/**
 * @tparam StorageAccess class to access the real storage (for declaration @see StorageAccessor class)
 * @tparam AddrRange     address range of the storage (for declaration @see AddrRange class)
 * @tparam T             types to store and accsess.
 *
 * @example
 * Declaration:
 *  enum StorageElems { param1, param2 };
 *  typedef StoreHelper<StorageElems> SH;
 *  Storage<StorageAccessor, AddrRange<1, 27>, SH::store<param1, int>, SH::store<param2, double> s;
 * Usage:
 *   int a = s.get<param1>();
 *   s.set<param2>(2.0);
 *
 */
template<typename StorageAccess, typename AddrRange, typename... T>
class Storage
{
  static const typename AddrRange::type addrMin = AddrRange::min;

  static_assert(detail::IdxValidator<0, T...>::isValid, "Wrong order of defined idx-type!" " Required order: 0-type 1-type...");
  static_assert(detail::MaxAddr<addrMin, T...>::value <= AddrRange::max, "Address space is to small to store all defined types!");

public:
    typedef typename detail::IdxType<T...>::type idxType;

    template<idxType idx> typename detail::StorableTraits<idx, addrMin, T...>::type get() {
        typedef detail::StorableTraits<idx, addrMin, T...> StorableTraits;
        return StorageAccess::template read<typename StorableTraits::type, StorableTraits::addr>();
    }
    template<idxType idx> void set(typename  detail::StorableTraits<idx, addrMin, T...>::type val) {
        typedef  detail::StorableTraits<idx, addrMin, T...> StorableTraits;
        StorageAccess::template write<typename StorableTraits::type, StorableTraits::addr>(val);
    }
};

} // namespaces utils
} // namespace avrtl

#endif // AVRTL_UTILS_STORAGE_HPP
