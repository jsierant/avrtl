/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_RATIO_HPP
#define AVRTL_UTILS_RATIO_HPP

extern "C"
{
#include <stdint.h>
}
#include "type_traits.hpp"

namespace {

template<typename T, T v> struct static_abs : avrtl::utils::integral_constant<T, ((v < 0) ? -v : v)> { };
template<typename T, T v> struct static_sign : avrtl::utils::integral_constant< T, ((v < 0) ? -1 : 1) > { };

template<intmax_t P, intmax_t Q> struct static_gcd : static_gcd<Q, (P % Q)> { };
template<intmax_t P> struct static_gcd<P, 0> : avrtl::utils::integral_constant<intmax_t, static_abs<intmax_t, P>::value> { };
template<intmax_t Q> struct static_gcd<0, Q> : avrtl::utils::integral_constant<intmax_t, static_abs<intmax_t, Q>::value> { };

} // namespace

namespace avrtl {
namespace utils {

template <intmax_t N, intmax_t D = 1>
struct ratio {
   static_assert(D != 0, "division by 0");
   typedef ratio<N, D> type;
   static constexpr intmax_t num = N* static_sign<intmax_t, D>::value / static_gcd<N, D>::value;
   static constexpr intmax_t den = static_abs<intmax_t, D>::value / static_gcd<N, D>::value;
};

//  SI typedefs
// typedef ratio<1, 1000000000000000000000000> yocto;
// typedef ratio<1,    1000000000000000000000> zepto;
typedef ratio<1,       1000000000000000000> atto;
typedef ratio<1,          1000000000000000> femto;
typedef ratio<1,             1000000000000> pico;
typedef ratio<1,                1000000000> nano;
typedef ratio<1,                   1000000> micro;
typedef ratio<1,                      1000> milli;
typedef ratio<1,                       100> centi;
typedef ratio<1,                        10> deci;
typedef ratio<                       10, 1> deca;
typedef ratio<                      100, 1> hecto;
typedef ratio<                     1000, 1> kilo;
typedef ratio<                  1000000, 1> mega;
typedef ratio<               1000000000, 1> giga;
typedef ratio<            1000000000000, 1> tera;
typedef ratio<         1000000000000000, 1> peta;
typedef ratio<      1000000000000000000, 1> exa;
// typedef ratio<   1000000000000000000000, 1> zetta;
// typedef ratio<1000000000000000000000000, 1> yotta;

template<typename S, typename D>
struct ratio_divide : ratio<S::num * D::den, S::den * D::num> {
   static_assert(D::num != 0, "division by 0");
};

} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_RATIO_HPP
