/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_FOR_EACH_TYPE_HPP
#define AVRTL_UTILS_FOR_EACH_TYPE_HPP

#include "enable_if.hpp"

namespace avrtl
{
namespace utils
{

template<class T, class R, class... Tail> typename utils::enable_if<!sizeof...(Tail)>::type for_each_type(T&) { }

template<class T, class R, class Head, class... Tail>
typename utils::enable_if<sizeof...(Tail) + 1>::type for_each_type(T& t)
{
    R::template execute<Head>(t);
    for_each_type<T, R, Tail...>(t);
}


template<class T, class... Tail> typename utils::enable_if<!sizeof...(Tail)>::type for_each_type() { }

template<class T, class Head, class... Tail>
typename utils::enable_if<sizeof...(Tail) + 1>::type for_each_type()
{
    T::template execute<Head>();
    for_each_type<T, Tail...>();
}

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_FOR_EACH_TYPE_HPP
