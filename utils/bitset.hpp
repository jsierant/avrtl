/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_BITSET_HPP
#define AVRTL_UTILS_BITSET_HPP

#include "bits_traits.hpp"

namespace avrtl
{
namespace utils
{

template<uint8_t tsize, typename T = typename smallest_type_to_store_bits<tsize>::type>
class bitset
{
public:
	static uint8_t const size = tsize;
	typedef T value_type;

	explicit bitset(T initVal) : v(initVal) {}
	bool operator[](T pos) const { return (T(1u) << pos) & v; }
private:
	T v;
};

}
}

#endif // AVRTL_UTILS_BITSET_HPP
