/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_BUTTON_HPP
#define AVRTL_UTILS_BUTTON_HPP

namespace avrtl {
namespace utils {

template<typename t_handler,
         typename t_pin_out,
         typename t_pin_in >
class button {
public:
   typedef t_handler handler_type;
   button(t_handler& handler) : m_handler(handler), m_is_pressed() { }
   void init() {
      t_pin_out::init();
      t_pin_in::init();
      t_pin_out::clear();
   }
   inline void check_press() const {
      m_is_pressed = !t_pin_in::isSet();
   }
   inline void handle_press() { if (m_is_pressed) { m_handler.handle(); m_is_pressed = false; } }
private:
   t_handler& m_handler;
   mutable bool m_is_pressed;
};

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_BUTTON_HPP
