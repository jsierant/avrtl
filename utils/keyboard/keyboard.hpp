/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_KEYBOARD_HPP
#define AVRTL_UTILS_KEYBOARD_HPP

#include "../for_each_type.hpp"

namespace avrtl {
namespace utils {

template<class... vt_buttons>
class keyboard : public vt_buttons... {
public:
   keyboard(typename vt_buttons::handler_type... handler) : vt_buttons(handler)... {}
   void init() { for_each_button<init_impl>(); }
   void check_keys_status() { for_each_button<check_keys_status_impl>(); }
   void handle_press() { for_each_button<handle_press_impl>(); }

private:
   template<typename impl> void for_each_button() { for_each_type<keyboard, impl, vt_buttons...>(*this); }

   struct check_keys_status_impl {
      template<class T> static void execute(keyboard& k) {
         static_cast<T&>(k).check_press();
      }
   };
   struct handle_press_impl {
      template<class T> static void execute(keyboard& k) {
         static_cast<T&>(k).handle_press();
      }
   };
   struct init_impl {
      template<class T> static void execute(keyboard& k) {
         static_cast<T&>(k).init();
      }
   };
};

} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_KEYBOARD_HPP
