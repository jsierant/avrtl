/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_STAT_STATIC_VECTOR_HPP
#define AVRTL_UTILS_STAT_STATIC_VECTOR_HPP

namespace avrtl {
namespace utils {
namespace stat {

template<typename T, T... vals> struct vector_c { typedef T value_type; };

} // namespaces stat
} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_STAT_STATIC_VECTOR_HPP
