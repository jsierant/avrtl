/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_FORCE_INLINE_HPP
#define AVRTL_FORCE_INLINE_HPP


#ifdef __GNUC__
#define force_inline __attribute__((always_inline))
#elif _MSC_VER
#define force_inline __forceinline
#else
#define force_inline inline
#endif

#endif //AVRTL_FORCE_INLINE_HPP
