/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_BINARYLOGIC_H
#define AVRTL_UTILS_BINARYLOGIC_H

namespace avrtl
{
namespace utils
{
namespace binary_logic
{

enum class level
{
	high = 1, low = 0
};

bool to_bool(level l ) { return l == level::high; }
level get_level(bool b) { return b ? level::high : level::low; }

} // namespace binary_logic
} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_BINARYLOGIC_H
