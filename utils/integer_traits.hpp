/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 *
 * Copyright (C) Jens Maurer 2000.
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 *
 * Based on boost integer_traits class (idea by Beman Dawes, Ed Brey, Steve Cleary, and Nathan Myers).
 */

#ifndef AVRTL_INTEGER_TRAITS_HPP
#define AVRTL_INTEGER_TRAITS_HPP

extern "C"
{
#include <stdint.h>
#include <limits.h>
}

namespace avrtl {
namespace utils {

namespace detail
{
template<typename T, T min_val, T max_val>
class integer_traits_base
{
public:
	static const bool is_integral = true;
	static const T const_min = min_val;
	static const T const_max = max_val;
	static const bool is_signed = (min_val < 0);
};

} // namespace detail

template<typename T>
class integer_traits
{
public:
	static const bool is_integral = false;
};

template<> class integer_traits<bool> : public detail::integer_traits_base<bool, false, true> { };
template<> class integer_traits<char> : public detail::integer_traits_base<char, CHAR_MIN, CHAR_MAX> { };

template<> class integer_traits<signed char> : public detail::integer_traits_base<signed char, SCHAR_MIN, SCHAR_MAX> { };
template<> class integer_traits<unsigned char> : public detail::integer_traits_base<unsigned char, 0, UCHAR_MAX> { };


template<> class integer_traits<short> : public detail::integer_traits_base<short, SHRT_MIN, SHRT_MAX> { };
template<> class integer_traits<unsigned short> : public detail::integer_traits_base<unsigned short, 0, USHRT_MAX> { };

template<> class integer_traits<int> : public detail::integer_traits_base<int, INT_MIN, INT_MAX> { };
template<> class integer_traits<unsigned int> : public detail::integer_traits_base<unsigned int, 0, UINT_MAX> { };

template<> class integer_traits<long> : public detail::integer_traits_base<long, LONG_MIN, LONG_MAX> { };
template<> class integer_traits<unsigned long> : public detail::integer_traits_base<unsigned long, 0, ULONG_MAX> { };

} // namespace utils
} // namespace avrtl

#endif /* AVRTL_INTEGER_TRAITS_HPP */
