/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_DETAIL_IS_RATIO_HPP
#define AVRTL_UTILS_DETAIL_IS_RATIO_HPP

#include "../utils/type_traits.hpp"
#include "../utils/ratio.hpp"

namespace avrtl {
namespace utils {
namespace detail {

template<typename T> struct is_ratio : false_type { };
template<intmax_t N, intmax_t D> struct is_ratio<ratio<N, D>> : true_type { };


} // namespaces detail
} // namespaces utils
} // namespaces avrtl

#endif // AVRTL_UTILS_DETAIL_IS_RATIO_HPP
