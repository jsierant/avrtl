/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_UTILS_DETAIL_IS_STATIC_DURATION_HPP
#define AVRTL_UTILS_DETAIL_IS_STATIC_DURATION_HPP

#include "../type_traits.hpp"
#include "../duration.hpp"

namespace avrtl {
namespace utils {
namespace detail {

template<typename D> struct is_static_duration : false_type {};
template<typename Rep, typename Period, Rep v> struct is_static_duration<stat::duration<Rep, Period, v> > : true_type { };

} // namespace detail
} // namespace utils
} // namespace avrtl

#endif // AVRTL_UTILS_DETAIL_IS_STATIC_DURATION_HPP
