/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_PORTPIN_HPP
#define AVRTL_DIGITALIO_PORTPIN_HPP

#include "defs.hpp"
#include "detail/pinmanipulator.hpp"
#include "detail/manipulatordefs.hpp"

#include "../utils/force_inline.hpp"

#include "pin_definers.hpp"

namespace avrtl
{
namespace dio
{

template <PortName tv_portName, PinIdx idx, typename t_pinType> class PortPin
	: private detail::PinManipilator<tv_portName, idx> {};


template <PortName tv_portName, PinIdx idx, InMode inMode>
class PortPin<tv_portName, idx, PinDef::In<inMode>> : private detail::PinManipilator<tv_portName, idx>
{
	typedef detail::PinManipilator<tv_portName, idx> HwPinCtrl;
public:
	using HwPinCtrl::isSet;
	force_inline static void init() { PinDef::In<inMode>::template init<HwPinCtrl>(); }
};


template <PortName tv_portName, PinIdx idx, utils::binary_logic::level l>
class PortPin<tv_portName, idx, PinDef::Out<l>> : private detail::PinManipilator<tv_portName, idx>
{
	typedef detail::PinManipilator<tv_portName, idx> HwPinCtrl;
public:
	using HwPinCtrl::isSet;
	using HwPinCtrl::set;
	using HwPinCtrl::clear;

	force_inline static void init() { PinDef::Out<l>::template init<HwPinCtrl>(); }
};

} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_PORTPIN_HPP
