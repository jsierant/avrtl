/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_PORT_HPP
#define AVRTL_DIGITALIO_PORT_HPP

#include "defs.hpp"
#include "portpin.hpp"
#include "detail/portmanipulator.hpp"
#include "detail/manipulatordefs.hpp"
#include "pin_definers.hpp"
#include "../utils/static_vector.hpp"
#include "../utils/static_algorithm.hpp"

namespace avrtl
{
namespace dio
{

template<PortName tv_portName, typename... PortDef>
class Port
{
	static_assert(sizeof...(PortDef) == 8, "Exactly 8 pins must be defined!");
	typedef typename utils::stat::at<0, PortDef...>::type Pin0Def;
	typedef typename utils::stat::at<1, PortDef...>::type Pin1Def;
	typedef typename utils::stat::at<2, PortDef...>::type Pin2Def;
	typedef typename utils::stat::at<3, PortDef...>::type Pin3Def;
	typedef typename utils::stat::at<4, PortDef...>::type Pin4Def;
	typedef typename utils::stat::at<5, PortDef...>::type Pin5Def;
	typedef typename utils::stat::at<6, PortDef...>::type Pin6Def;
	typedef typename utils::stat::at<7, PortDef...>::type Pin7Def;

public:
  typedef PortPin<tv_portName, pin0, Pin0Def> Pin0;
  typedef PortPin<tv_portName, pin1, Pin1Def> Pin1;
  typedef PortPin<tv_portName, pin2, Pin2Def> Pin2;
  typedef PortPin<tv_portName, pin3, Pin3Def> Pin3;
  typedef PortPin<tv_portName, pin4, Pin4Def> Pin4;
  typedef PortPin<tv_portName, pin5, Pin5Def> Pin5;
  typedef PortPin<tv_portName, pin6, Pin6Def> Pin6;
  typedef PortPin<tv_portName, pin7, Pin7Def> Pin7;
};


template<PortName tv_portName, utils::binary_logic::level l> class Port<tv_portName, PinDef::AllOut<l>>
{
public:
  typedef PortPin<tv_portName, pin0, PinDef::Out<l>> Pin0;
  typedef PortPin<tv_portName, pin1, PinDef::Out<l>> Pin1;
  typedef PortPin<tv_portName, pin2, PinDef::Out<l>> Pin2;
  typedef PortPin<tv_portName, pin3, PinDef::Out<l>> Pin3;
  typedef PortPin<tv_portName, pin4, PinDef::Out<l>> Pin4;
  typedef PortPin<tv_portName, pin5, PinDef::Out<l>> Pin5;
  typedef PortPin<tv_portName, pin6, PinDef::Out<l>> Pin6;
  typedef PortPin<tv_portName, pin7, PinDef::Out<l>> Pin7;
};
} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_PORT_HPP
