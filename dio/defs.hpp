/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIO_DEFS_HPP
#define AVRTL_DIO_DEFS_HPP

#include <stdint.h>

namespace avrtl
{
namespace dio
{

/**
 * Available port names - no protection against not-existing port
 */
enum PortName : uint8_t { PortA, PortB, PortC, PortD };

/**
 * Pin idx in port
 */
enum PinIdx : uint8_t
{
  pin0 = (1 << 0x00),
  pin1 = (1 << 0x01),
  pin2 = (1 << 0x02),
  pin3 = (1 << 0x03),
  pin4 = (1 << 0x04),
  pin5 = (1 << 0x05),
  pin6 = (1 << 0x06),
  pin7 = (1 << 0x07)
};

/**
 * Type of configurations of pin defiand as IN
 */
enum Type : uint8_t
{
  standard, //!< standard configuration - pin is in low state
  pulled_up //!< pin is pulled up to high state with internal resitor
};

enum class InMode : uint8_t
{
  standard, //!< standard configuration - pin is in low state
  pulled_up //!< pin is pulled up to high state with internal resitor
};

} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIO_DEFS_HPP
