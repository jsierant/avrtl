/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_PIN_HPP
#define AVRTL_DIGITALIO_PIN_HPP

#include "defs.hpp"

#include "../utils/binary_logic.hpp"
#include "../utils/force_inline.hpp"
#include "../utils/type_traits.hpp"
namespace avrtl
{
namespace dio
{

namespace PinDef
{

template<utils::binary_logic::level initPinLevel = utils::binary_logic::level::low>
struct Out
{
	template <typename HwPinCtrl> force_inline static void init() {
		HwPinCtrl::setAsOut();
		if(initPinLevel == utils::binary_logic::level::high) HwPinCtrl::set();
	}
};

template<InMode inMode = InMode::standard>
struct In
{
	template <typename HwPinCtrl> force_inline static void init() { HwPinCtrl::template setAsIn<inMode>(); }
};

struct Unmanaged
{
   force_inline static void init() {}
};

template<utils::binary_logic::level = utils::binary_logic::level::low>
struct AllOut { };


} // namespace PinDef

template<typename P> struct is_out : utils::false_type {};
template<template <PortName, PinIdx, typename> class T, utils::binary_logic::level l, PortName pn, PinIdx idx>
	struct is_out<T<pn, idx, PinDef::Out<l> >> : utils::true_type {};


template<typename P> struct is_in : utils::false_type {};
template<template <PortName, PinIdx, typename> class T, InMode m, PortName pn, PinIdx idx>
	struct is_in<T<pn, idx, PinDef::In<m> >> : utils::true_type {};


} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_PIN_HPP
