/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_MANIPULATORDEFS_HPP
#define AVRTL_DIGITALIO_MANIPULATORDEFS_HPP

extern "C" {
#include <avr/io.h>
}


#include "portmanipulator.hpp"
#include "pinmanipulator.hpp"

namespace avrtl
{
namespace dio
{
namespace detail
{

DEFINE_PORT_MANIPULATOR(A)
DEFINE_PORT_MANIPULATOR(B)
DEFINE_PORT_MANIPULATOR(C)
DEFINE_PORT_MANIPULATOR(D)

DEFINE_PIN_MANIPULATOR(A)
DEFINE_PIN_MANIPULATOR(B)
DEFINE_PIN_MANIPULATOR(C)
DEFINE_PIN_MANIPULATOR(D)

} // namespace detail
} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_MANIPULATORDEFS_HPP
