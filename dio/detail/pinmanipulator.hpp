/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_PINMANIPULATOR_HPP
#define AVRTL_DIGITALIO_PINMANIPULATOR_HPP

#include "../defs.hpp"
#include "../../utils/force_inline.hpp"
#include "../../utils/enable_if.hpp"
#include "../../utils/binary_logic.hpp"

namespace avrtl
{
namespace dio
{
namespace detail
{

template<PortName tv_port, PinIdx tv_idx>
struct PinManipilator
{
	force_inline static void setAsOut();
	force_inline static void setAsIn(Type type);
	force_inline static bool isSet();
	force_inline static void set(utils::binary_logic::level);
	force_inline static bool clear();
};

#define DEFINE_PIN_MANIPULATOR(PNAME) \
template<PinIdx tv_idx> \
struct PinManipilator<Port##PNAME, tv_idx> \
{ \
	force_inline static void setAsOut() { DDR##PNAME |= tv_idx; } \
\
	template <Type tv_type> force_inline static typename ::avrtl::utils::enable_if<tv_type == Type::pulled_up>::type \
	setAsIn() { DDR##PNAME &= ~uint8_t(tv_idx); set(); } \
	template <Type tv_type> force_inline static typename ::avrtl::utils::enable_if<tv_type == Type::standard >::type \
	setAsIn() { DDR##PNAME &= ~uint8_t(tv_idx); } \
\
	force_inline static bool isSet() { return (PIN##PNAME & uint8_t(tv_idx)); } \
	force_inline static void set(utils::binary_logic::level l = utils::binary_logic::level::high) { \
		if(utils::binary_logic::to_bool(l)) { \
			PORT##PNAME |= tv_idx;\
		} else {\
			clear();\
		} \
	} \
	force_inline static void clear() { PORT##PNAME &= ~tv_idx; } \
};

} // namespace detail
} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_PINMANIPULATOR_HPP
