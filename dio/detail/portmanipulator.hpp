/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DIGITALIO_PORTMANIPULATOR_HPP
#define AVRTL_DIGITALIO_PORTMANIPULATOR_HPP

#include "../defs.hpp"

#include "../../utils/force_inline.hpp"
#include "../../utils/enable_if.hpp"

namespace avrtl
{
namespace dio
{
namespace detail
{

template<PortName tv_portName>
struct PortManipilator
{
	force_inline static void setAsOut();
	force_inline static void setAsIn(Type type);
	force_inline static bool clear();
	force_inline static bool set();
};

#define DEFINE_PORT_MANIPULATOR(PNAME) \
template<> \
struct PortManipilator<Port##PNAME> \
{ \
	force_inline static void setAsOut() { DDR##PNAME = 0xff; } \
\
	template <Type tv_type> force_inline static typename ::avrtl::utils::enable_if<tv_type == Type::pulled_up>::type \
	setAsIn() { DDR##PNAME = 0x00; set(); } \
	template <Type tv_type> force_inline static typename ::avrtl::utils::enable_if<tv_type == Type::standard >::type \
	setAsIn() { DDR##PNAME = 0x00; } \
\
	force_inline static void clear() { PORT##PNAME = 0x00;} \
	force_inline static void set() { PORT##PNAME = 0xff;} \
};

} // namespace detail
} // namespace dio
} // namespace avrtl

#endif // AVRTL_DIGITALIO_PORTMANIPULATOR_HPP
