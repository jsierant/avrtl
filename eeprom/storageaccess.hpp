/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_EEPROM_STORAGEACCESS_HPP
#define AVRTL_EEPROM_STORAGEACCESS_HPP

#include "../utils/storage.hpp"

extern "C" {
#include <avr/eeprom.h>
}

namespace avrtl
{
namespace eeprom
{

class StorageAccess
{
public:
    template<typename T, utils::Address Addr>
    static T read() {
        T val;
        eeprom_read_block(&val, reinterpret_cast<void*>(Addr), sizeof(T));
        return val;
    }
    template<typename T, utils::Address Addr>
    static void write(T val) {
        eeprom_write_block(&val, reinterpret_cast<void*>(Addr), sizeof(T));
    }
};
} // namespaces eeprom
} // namespace avrtl

#endif // AVRTL_EEPROM_STORAGEACCESS_HPP
