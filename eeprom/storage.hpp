/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_EEPROM_STORAGE_HPP
#define AVRTL_EEPROM_STORAGE_HPP

#include "../utils/storage.hpp"
#include "storageaccess.hpp"
#include "addrrange.hpp"

namespace avrtl
{
namespace eeprom
{

template<typename... Elems>
class Storage : public utils::Storage<StorageAccess, AddrRange, Elems...> { };

} // namespaces eeprom
} // namespace avrtl

#endif // AVRTL_EEPROM_STORAGE_HPP
