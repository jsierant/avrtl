/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_EEPROM_ADDRRANGE_HPP
#define AVRTL_EEPROM_ADDRRANGE_HPP

#include "../utils/storage.hpp"

namespace avrtl
{
namespace eeprom
{

namespace
{
struct MaxAddr
{
constexpr static const uintptr_t value =
#if defined (__AVR_ATmega8__)
  511
#elif defined (__AVR_ATmega16__) || defined (__AVR_ATmega32__)
  1023
#endif
  ;
};


} // namespace

struct AddrRange : public utils::AddrRange<0u, MaxAddr::value> { };

} // namespaces eeprom
} // namespace avrtl

#endif // AVRTL_EEPROM_ADDRRANGE_HPP
