/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_DELAY_SLEEP_FOR_HPP
#define AVRTL_DELAY_SLEEP_FOR_HPP

extern "C"
{
#include <stdint.h>
}

#include "../units/duration.hpp"
#include "../units/detail/is_duration.hpp"

#include "../utils/force_inline.hpp"
#include "../utils/forward.hpp"
#include "../cpu_freq.hpp"

extern "C" void __builtin_avr_delay_cycles(uint32_t);

namespace avrtl {
namespace delay {

namespace {

void sleep_for_cycles(uint32_t cycles)
{
   const uint16_t precision = 100*100; // 0.01%
   const uint16_t step = units::unit_cast<units::Hz>(cpu_freq).val()/precision;
   uint32_t iters = cycles/step;
   while (iters--) __builtin_avr_delay_cycles(step);
}

template<typename D>
force_inline constexpr uint32_t calc_cycles(D&& duration)
{
   typedef typename D::period Period;
   return static_cast<uint32_t>(uint64_t(units::unit_cast<units::Hz>(cpu_freq).val())*duration.count()/Period::den);
}

} // namespace

template<typename D> inline void sleep_for(D&& duration)
{
   static_assert(units::detail::is_duration<D>::value, "Parameter for sleep_for shall be duration!");
   sleep_for_cycles(calc_cycles<D>(utils::forward<D>(duration)));
}

} // namespaces delay
} // namespaces avrtl

#endif // AVRTL_DELAY_SLEEP_FOR_HPP
