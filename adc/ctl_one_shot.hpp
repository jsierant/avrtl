/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_CTL_ONE_SHOT_HPP
#define AVRTL_ADC_CTL_ONE_SHOT_HPP

#include "partial_ctl/meas_ctl.hpp"
#include "partial_ctl/channel_selector.hpp"
#include "partial_ctl/interrupts_ctl.hpp"
#include "partial_ctl/meas_reader.hpp"
#include "partial_ctl/sample_rate_ctl.hpp"

namespace avrtl {
namespace adc {

template<typename T>
class ctl_one_shot : public partial_ctl::meas_ctl,
							public partial_ctl::channel_selector,
							public partial_ctl::interrupts_ctl,
							public partial_ctl::meas_reader<T>,
							public avrtl::adc::partial_ctl::sampling_rate_ctl {
public:
	static void init() {
		avrtl::adc::partial_ctl::sampling_rate_ctl::init();

		ADMUX |= (1 << REFS0); // Set ADC reference to AVCC
		ADMUX |= (1 << ADLAR); // Left adjust ADC result to allow easy 8 bit reading

		ADCSRA |= (1 << ADEN);  // Enable ADC
		partial_ctl::meas_reader<T>::init();
	}
};
} // namespaces adc
} // namespace avrtl

#endif // AVRTL_ADC_CTL_ONE_SHOT_HPP
