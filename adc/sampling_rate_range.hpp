/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_SAMPLING_RATE_RANGE_HPP
#define AVRTL_ADC_SAMPLING_RATE_RANGE_HPP

#include "../units/range.hpp"
#include "../units/frequency.hpp"

namespace avrtl {
namespace adc {

constexpr units::range<units::kHz> const sampling_rate_range{units::kHz(50), units::kHz(200)};

} // namespaces adc
} // namespaces avrtl

#endif // AVRTL_ADC_SAMPLING_RATE_RANGE_HPP
