/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_RAW_MEAS_PROVIDER_HPP
#define AVRTL_ADC_RAW_MEAS_PROVIDER_HPP

extern "C" {
#include <stdint.h>
}

#include "meas_updater.hpp"
#include "ctl_free_running.hpp"
#include "ctl_one_shot.hpp"

namespace avrtl {
namespace adc {

template <typename channels, typename controller>
class raw_meas_provider {
	typedef meas_updater<controller, channels> meas_updater_t;
public:
	typedef controller ctl_type;
	typedef typename ctl_type::value_type value_type;
	typedef channels channels_type;

	raw_meas_provider() { meas_updater_t::init(); }

	template<io::meas_channel channel> value_type get() const {
		return meas_updater_t::meas_storage_reader::template get<channel>();
	}
};

} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_RAW_MEAS_PROVIDER_HPP
