/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_IO_MEAS_CHANNEL_HPP
#define AVRTL_ADC_IO_MEAS_CHANNEL_HPP

namespace avrtl {
namespace adc {
namespace io {

enum meas_channel : uint8_t {
	ADC0 = 0x00, ADC1 = 0x01 // register values in ACDMUX
};

} // namespace io
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_IO_MEAS_CHANNEL_HPP
