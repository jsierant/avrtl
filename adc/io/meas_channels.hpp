/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef ADC_IO_MEAS_CHANNELS_HPP
#define ADC_IO_MEAS_CHANNELS_HPP

#include "meas_channel.hpp"
#include "../../utils/static_vector.hpp"

namespace avrtl {
namespace adc {
namespace io {

template<meas_channel... channels>
struct meas_channels : utils::stat::vector_c<meas_channel, channels...> {
	enum { size = sizeof...(channels) };
	typedef avrtl::utils::stat::vector_c<meas_channel, channels...> raw_type;
};

} // namespaces io
} // namespaces adc
} // namespace avrtl

#endif // ADC_IO_MEAS_CHANNELS_HPP
