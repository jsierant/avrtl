/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_IO_DETAIL_IS_MEAS_CHANNELS_HPP
#define AVRTL_ADC_IO_DETAIL_IS_MEAS_CHANNELS_HPP

#include "../../../utils/type_traits.hpp"
#include "../meas_channels.hpp"

namespace avrtl {
namespace adc {
namespace io {

namespace detail {
template<typename T, typename Channels> struct is_meas_channels : utils::false_type { };

template<typename T, T... values>
struct is_meas_channels<T, io::meas_channels<values...>> : utils::true_type { };

} // namespace detail
} // namespace io
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_IO_DETAIL_IS_MEAS_CHANNELS_HPP
