/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_MEAS_UPDATER_HPP
#define AVRTL_ADC_MEAS_UPDATER_HPP

#include "io/meas_channel.hpp"
#include "io/detail/is_meas_channels.hpp"
#include "meas_storage.hpp"

namespace avrtl {
namespace adc {

template<typename controller,
			typename Channels,
			uint8_t size = Channels::size>
class meas_updater {
	static_assert(io::detail::is_meas_channels<typename Channels::value_type, Channels>::value,
					  "The template parameter Channels shall be type of io::MeasChannels<...>!!!");
public:
	struct meas_storage_reader {
		template<io::meas_channel channel> static typename controller::value_type get()
		{ return meas_updater::storage.get<channel>(); }
	};

	static void init() {
		controller::init();
		controller::activate_interupts();
		controller::activate();
	}

	static void update() {
		storage.set(currentActiveChannelIdx, controller::read_measurement());
		select_next_channel();
		controller::activate();
	}
private:
	typedef meas_storage<typename controller::value_type, Channels> meas_storage_t;

	friend meas_storage_reader;

	static void select_next_channel() {
		currentActiveChannelIdx = (currentActiveChannelIdx + 1) % meas_storage_t::size;
		controller::select_channel(meas_storage_t::idx2channel(currentActiveChannelIdx));
	}

	static meas_storage_t storage;
	static uint8_t currentActiveChannelIdx;
};

template<typename controller,
			typename Channels,
			uint8_t size>
typename meas_updater<controller, Channels, size>::meas_storage_t
	meas_updater<controller, Channels, size>::storage = meas_updater<controller, Channels, size>::meas_storage_t();

template<typename controller,
			typename Channels,
			uint8_t size>
uint8_t meas_updater<controller, Channels, size>::currentActiveChannelIdx = 0u;


template<typename controller,
			typename Channels>
class meas_updater<controller, Channels, 1> {
	static_assert(io::detail::is_meas_channels<typename Channels::value_type, Channels>::value,
					  "The template parameter Channels shall be type of io::MeasChannels<...>!!!");
public:
	struct meas_storage_reader {
		template<io::meas_channel channel> static typename controller::value_type get() {
			static_assert(utils::stat::contains_c<Channels,
															  utils::stat::eq_c<io::meas_channel, channel>>::value,
							  "The requested channel has not been registered!!!");
			return controller::read_measurement();
		}
	};

	static void init() { controller::init(); }
};

template<typename controller, typename Channels>
class meas_updater<controller, Channels, 0>
{ };


#define REGISTER_UPDATER_FOR_ADC_INTERUPTS(provider_type) \
ISR(ADC_vect) \
{ \
	avrtl::adc::meas_updater<typename provider_type::ctl_type, typename provider_type::channels_type>::update(); \
}

} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_MEAS_UPDATER_HPP
