/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_MEAS_STORAGE_HPP
#define AVRTL_ADC_MEAS_STORAGE_HPP

#include "io/meas_channels.hpp"
#include "../utils/static_algorithm.hpp"

namespace avrtl {
namespace adc {

template<typename T,
			typename Channels>
class meas_storage;

template<typename T,
			template <io::meas_channel...> class Channels,
			io::meas_channel... channels>
class meas_storage<T, Channels<channels...>> {
public:
	enum : uint8_t { size = Channels<channels...>::size };

	template<io::meas_channel channel> T get() {
		return measurements[utils::stat::find_first_c<typename Channels<channels...>::raw_type,
								  utils::stat::eq_c<io::meas_channel, channel>>::value];
	}

	void set(uint8_t idx, T val) { measurements[idx] = val; }

	static io::meas_channel idx2channel(uint8_t idx) {
		static const io::meas_channel channelsList[size] = { channels... };
		return channelsList[idx];
	}

private:
	volatile T measurements[size];
};

} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_MEAS_STORAGE_HPP
