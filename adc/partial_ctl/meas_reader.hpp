/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_PARTIAL_CTL_MEAS_READER_HPP
#define AVRTL_ADC_PARTIAL_CTL_MEAS_READER_HPP

namespace avrtl {
namespace adc {
namespace partial_ctl {

namespace detail {

template<typename T, T t>
struct common_meas_reader {
	typedef T value_type; static constexpr value_type top = t;
};

} // namespace detail

template<typename T> struct meas_reader;

template<>
struct meas_reader<uint8_t> : detail::common_meas_reader<uint8_t, 0x00ff> {
public:
	static value_type read_measurement() { return ADCH; }
protected:
	static void init()
	{
		ADMUX |= (1 << ADLAR); // Left adjust ADC result to allow easy 8 bit reading
	}
};

} // namespace partial_ctl
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_PARTIAL_CTL_MEAS_READER_HPP
