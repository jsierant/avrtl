/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_PARTIAL_CTL_CHANNEL_SELECTOR_HPP
#define AVRTL_ADC_PARTIAL_CTL_CHANNEL_SELECTOR_HPP

#include "../io/detail/is_meas_channels.hpp"

namespace avrtl {
namespace adc {
namespace partial_ctl {

class channel_selector
{
public:
	static void select_channel(io::meas_channel p_channel) { ADMUX &= 0xf0; ADMUX |= p_channel; }
};

} // namespace partial_ctl
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_PARTIAL_CTL_CHANNEL_SELECTOR_HPP
