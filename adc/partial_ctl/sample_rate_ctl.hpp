/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_PARTIAL_CTL_SAMPLE_RATE_CTL_HPP
#define AVRTL_ADC_PARTIAL_CTL_SAMPLE_RATE_CTL_HPP

#include "../../cpu_freq.hpp"
#include "../../utils/static_vector.hpp"
#include "../../utils/static_algorithm.hpp"
#include "../sampling_rate_range.hpp"


namespace avrtl {
namespace adc {
namespace partial_ctl {

struct sampling_rate_ctl {
protected:
	typedef utils::stat::vector_c<uint8_t,    2,    4 ,   8 ,  16,   32,   64,  128> dividers;
	typedef utils::stat::vector_c<uint8_t, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07> register_masks;
	struct is_in_sampling_rate_range {
		template<uint8_t den> static constexpr bool apply() {
         return avrtl::units::in_range(sampling_rate_range, cpu_freq/decltype(cpu_freq)(den));
		}
	};

	static void init() {
		using namespace avrtl::utils::stat;
		ADCSRA &= ~0x07;
		ADCSRA |= at_c<register_masks, find_first_c<dividers,
																  is_in_sampling_rate_range>::value>::value; }
};

} // namespace partial_ctl
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_PARTIAL_CTL_SAMPLE_RATE_CTL_HPP
