/*
 * avrtl - AVR Template Library
 * Copyright (C) 2014  Jarosław Sierant <jaroslaw.sierant@gmail.com>
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef AVRTL_ADC_PARTIAL_CTL_MEAS_CTL_HPP
#define AVRTL_ADC_PARTIAL_CTL_MEAS_CTL_HPP

namespace avrtl {
namespace adc {
namespace partial_ctl {

class meas_ctl
{
public:
	static void activate() { ADCSRA |= (1 << ADSC); }
	static void deactivate() { ADCSRA &= ~(1 << ADSC); }
};

} // namespace partial_ctl
} // namespace adc
} // namespace avrtl

#endif // AVRTL_ADC_PARTIAL_CTL_MEAS_CTL_HPP
